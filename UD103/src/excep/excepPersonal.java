package excep;

@SuppressWarnings("serial")
public class excepPersonal extends Exception {
	protected String message;
	
	public excepPersonal() {
		this.message="";
	}
	
	public excepPersonal(String message) {
		this.message=message;
	}

	public String toString() {
		return message;
	}
}
